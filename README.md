audisp-json
===

An audispd plugin meant to sent JSON-formatted logs to syslog.

If you use it, you should probably disable the default audispd
syslog plugin.

