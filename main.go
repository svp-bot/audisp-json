package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"log/syslog"
	"os"
	"strings"
	"time"

	"github.com/elastic/go-libaudit"
	"github.com/elastic/go-libaudit/aucoalesce"
	"github.com/elastic/go-libaudit/auparse"
)

var (
	reasmBufferSize = flag.Int("reassembler-buffer", 8192, "reassembler buffer size")
	reasmHorizon    = flag.Duration("reassembler-timeout", 10*time.Second, "reassembled EOE event timeout")
)

func main() {
	log.SetFlags(0)
	flag.Parse()

	if err := processLogs(); err != nil {
		log.Fatalf("error: %v", err)
	}
}

func processLogs() error {
	output, err := syslog.Dial("", "", syslog.LOG_INFO|syslog.LOG_AUTH, "audit")
	if err != nil {
		return err
	}
	defer output.Close() // nolint

	reassembler, err := libaudit.NewReassembler(*reasmBufferSize, *reasmHorizon, &streamHandler{output})
	if err != nil {
		return fmt.Errorf("failed to create reassembler: %v", err)
	}
	defer reassembler.Close() // nolint

	// Start goroutine to periodically purge timed-out events.
	go func() {
		t := time.NewTicker(500 * time.Millisecond)
		defer t.Stop()
		for range t.C {
			if reassembler.Maintain() != nil {
				return
			}
		}
	}()

	// Process lines from standard input.
	s := bufio.NewScanner(os.Stdin)
	for s.Scan() {
		line := s.Text()

		// Remove anything before the log type.
		p := strings.Index(line, "type=")
		if p < 0 {
			continue
		} else if p > 0 {
			line = line[p:]
		}

		auditMsg, err := auparse.ParseLogLine(line)
		if err != nil {
			log.Printf("failed to parse message header: %v", err)
			continue
		}

		reassembler.PushMessage(auditMsg)
	}

	return nil
}

type streamHandler struct {
	w io.Writer
}

func (s *streamHandler) ReassemblyComplete(msgs []*auparse.AuditMessage) {
	event, err := aucoalesce.CoalesceMessages(msgs)
	if err != nil {
		log.Printf("warning: failed to coalesce messages: %v", err)
		return
	}

	aucoalesce.ResolveIDs(event)

	if err := printJSON(s.w, event); err != nil {
		log.Printf("warning: failed to marshal event to JSON: %v", err)
	}
}

func (s *streamHandler) EventsLost(count int) {
	log.Printf("detected the loss of %v sequences", count)
}

func printJSON(w io.Writer, v interface{}) error {
	lbuf := []byte("@cee:")
	jsonBytes, err := json.Marshal(v)
	if err != nil {
		return err
	}
	lbuf = append(lbuf, jsonBytes...)
	_, err = w.Write(lbuf)
	return err
}
